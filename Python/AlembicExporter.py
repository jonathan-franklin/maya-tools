import maya.cmds as cmds
import maya.OpenMayaAnim as animAPI

class AlembicExporter:
    def __init__(self):
        pass

    def create(self):
        self.BuildLayout()

    def BuildLayout(self):

        self.window = cmds.window(widthHeight=(400,300), title="Alembic Exporter", resizeToFitChildren=1)
        cmds.columnLayout(rowSpacing=5)
        self.pathField = cmds.textField(placeholderText="E:\Git Repos\senior-kaiju-film",width=200)
        cmds.text(label="Repository Path:")
        cmds.button(label="Execute",command=lambda x: self.CheckRepoField())
        cmds.setParent('..')
        cmds.setParent('..')
        
        #allowedAreas = ['right', 'left']
        #cmds.dockControl( area='left', content=myWindow, allowedArea=allowedAreas )
        cmds.showWindow(self.window)

    def Export(self, repoDirectory):
        selection = cmds.ls(geometry=True) # Restrict by characters
        start = animAPI.MAnimControl.minTime().value()
        end = animAPI.MAnimControl.maxTime().value()
        root = "PrincessTower:Zilla:Zilla"
        holding = repoDirectory.split('\\')
        newRepo = ""
        for x in holding:
            newRepo += x + "\\\\"
        repoDirectory = newRepo
        print("Repo: " + repoDirectory)

        save_name = "\"" + repoDirectory + "Senior Project Big Files\\\Animation\\\Alembic\\\\alembicTest.abc\""

        command = "-frameRange " + str(int(start)) + " " + str(int(end)) +" -uvWrite -worldSpace -writeUVSets -stripNamespaces -renderableOnly" + " -root PrincessTower:Zilla:Body_highPoly_9_28_geo -file " + save_name
        print(command)
        cmds.AbcExport ( j = command )

    def CheckRepoField(self):
        repoInput = cmds.textField(self.pathField, query=1, text=1)
        self.Export(repoInput)

test = AlembicExporter()
test.create()